from .models import Attendee, ConferenceVO
from common.json import ModelEncoder


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",

    ]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}


class ConferenceVOEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]
